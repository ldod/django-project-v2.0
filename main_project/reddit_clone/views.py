from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from datetime import datetime
from .models import Post, Comment
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

def post_list(request):
    post_list = Post.published.all()
    query = request.GET.get('q')
    if query:
        post_list = Post.published.filter(title__icontains=query)

    paginator = Paginator(post_list, 10)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)


    if page is None:
        start_index=0
        end_index =5
    else:
        (start_index,end_index) = proper_pagination(posts,index=3)
    
    page_range = list(paginator.page_range)[start_index:end_index]

    context = {
        'posts':posts,
        'page_range':page_range,
    }
    return render(request,'reddit_clone/post_list.html',context)


def proper_pagination(posts,index):
    start_index=0
    end_index=5
    if posts.number >index:
        start_index = posts.number - index
        end_index = start_index + end_index
    return(start_index, end_index)



def post_detail(request,id,slug):
    post = get_object_or_404(Post,id=id,slug=slug)
    comments = Comment.objects.filter(post=post,reply=None).order_by('-id')

    # if post.votes.filter(id=request.user.id).exists():
    #     is_voted = True
    if request.method == 'POST':
        comment_form=CommentForm(request.POST or None)
        if comment_form.is_valid():
            content = request.POST.get('content')
            reply_id = request.POST.get('comment_id')
            comment_qs = None
            if reply_id:
                comment_qs=Comment.objects.get(id=reply_id)
            comment=Comment.objects.create(post=post,user=request.user,content=content,reply=comment_qs)
            comment.save()
            return HttpResponseRedirect(post.get_absolute_url())
    else:
        comment_form=CommentForm()

    context = {
        'post':post,
        'comments':comments,
        'comment_form':comment_form,
    }
    return render(request,'reddit_clone/post_detail.html',context)

def comment_edit(request,id):
    post = get_object_or_404(Post,id=id)
    comment=get_object_or_404(Comment,id=id)
    if request.user.id != comment.user_id:
        raise Http404()
    if request.method == "POST":
        comment_form = CommentEditForm(request.POST or None, instance=comment)
        if comment_form.is_valid():
            comment.save()
            return HttpResponseRedirect(reverse('post_list'))
    else:
        comment_form = CommentEditForm(instance=comment)
    context = {
        'post':post,
        'comment':comment,
        'comment_form':comment_form,
    }
    return render(request,'reddit_clone/comment_edit.html',context)

def comment_delete(request, id):
    post = get_object_or_404(Post,id=id)
    comment=get_object_or_404(Comment,id=id)
    if request.user.id != comment.user_id:
        raise Http404()
    comment.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))



# def vote_post(request):
#     post = get_object_or_404(Post,id=request.POST.get('post_id'))
#     is_voted=False
#     if post.votes.filter(id=request.user.id).exists():
#         post.votes.remove(request.user)
#         is_voted=False
#     else:
#         post.votes.add(request.user)
#         is_voted=True
#    return HttpResponseRedirect(post.get_absolute_url())

def upvote(request,id):
    return cast_vote(request,id,+1)

def downvote(request,id):
    return cast_vote(request,id,-1)

def cast_vote(request,id,score):
    post = get_object_or_404(Post,id=id)
    post.votes+=score
    post.save()
    return HttpResponseRedirect(post.get_absolute_url())


def post_create(request):
    if request.method == 'POST':
        form = PostCreateForm(request.POST)
        if form.is_valid():
            post=form.save(commit=False)
            post.author = request.user
            post.save()
    else:
        form = PostCreateForm()
    context = {
        'form':form,
    }
    return render(request,'reddit_clone/post_create.html',context)

def user_login(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username,password=password)
            if user:
                if user.is_active:
                    login(request,user)
                    return HttpResponseRedirect(reverse('post_list'))
                else:
                    return HttpResponse("User is not active")
            else:
                return HttpResponse("User with that name/password doesn't exist!")
    else:
        form =UserLoginForm()
    
    context = {
        'form':form,
    }
    return render(request, 'reddit_clone/login.html',context)


def user_logout(request):
    logout(request)
    return redirect('post_list')

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            return redirect('post_list')
    else:
        form = UserRegistrationForm()
    context = {
        'form': form,
    }
    return render(request,'registration/register.html',context)



def post_edit(request,id):
    post = get_object_or_404(Post,id=id)
    if post.author != request.user:
        raise Http404()
    if request.method == "POST":
        form = PostEditForm(request.POST or None, instance=post)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(post.get_absolute_url())
    else:
        form = PostEditForm(instance=post)
    context = {
        'form':form,
        'post':post,
    }
    return render(request,'reddit_clone/post_edit.html',context)

def post_delete(request, id):
    post=get_object_or_404(Post,id=id)
    if request.user != post.author:
        raise Http404()
    post.delete()
    return redirect('post_list')
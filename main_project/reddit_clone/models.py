from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify

# Create your models here.

class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager,self).get_queryset().filter(status="published")


class Post(models.Model):
    objects = models.Manager()
    published = PublishedManager()


    STATUS_CHOICES = (
        ('draft','Draft'),
        ('published','Published'),
    )

    title   =   models.CharField(max_length=100)
    slug    =   models.SlugField(max_length=120)
    author  =   models.ForeignKey(User,on_delete=models.CASCADE,related_name='reddit_posts')
    body    =   models.TextField()
    votes   =   models.IntegerField(default=0)
    created =   models.DateTimeField(auto_now_add=True)
    updated =   models.DateTimeField(auto_now=True)
    status  =   models.CharField(max_length=10,choices=STATUS_CHOICES,default='draft')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("reddit_clone:post_detail", args=[self.id,self.slug])



@receiver(pre_save,sender=Post)
def pre_save_slug(sender, **kwargs):
    slug=slugify(kwargs['instance'].title)
    kwargs['instance'].slug=slug

class Comment(models.Model):
    post=models.ForeignKey(Post,on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    reply = models.ForeignKey('self',on_delete=models.CASCADE ,null=True,related_name="replies")
    content = models.TextField(max_length=160)
    timestamp=models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return '{}-{}'.format(self.post.title,str(self.user.username))
